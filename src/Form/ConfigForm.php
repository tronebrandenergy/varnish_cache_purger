<?php

namespace Drupal\varnish_cache_purger\Form;

use Drupal\Core\Form\{ ConfigFormBase, FormStateInterface };


class ConfigForm extends ConfigFormBase {

    /**
     * @return string
     */
    public function getFormId() : string {
        return "varnish_cache_purger_settings";
    }

    /**
     * @return array
     */
    public function getEditableConfigNames() : array {
        return [
            "varnish_cache_purger.settings"
        ];
    }

    /**
     * @param array $form
     * @param FormStateInterface $formState
     * @return array
     */
    public function buildForm(array $form, ?FormStateInterface $formState) : array {
        $config = \Drupal::config("varnish_cache_purger.settings");

        $form["varnish_cache_purger_settings"]["salt"] = [
            "#type" => "textfield",
            "#title" => $this->t("Hash Salt"),
            "#default_value" => $config->get("salt"),
            "#required" => TRUE
        ];

        $form["varnish_cache_purger_settings"]["token"] = [
            "#type" => "textfield",
            "#title" => $this->t("Hash Token"),
            "#default_value" => $config->get("token"),
            "#required" => TRUE
        ];

        $form["varnish_cache_purger_settings"]["ban_endpoint"] = [
            "#type" => "textfield",
            "#title" => $this->t("BAN Endpoint"),
            "#default_value" => $config->get("ban_endpoint"),
            "#required" => FALSE
        ];

        $form["varnish_cache_purger_settings"]["algorithm"] = [
            "#type" => "select",
            "#title" => $this->t("Hash Algorithm"),
            "#options" => [
                "sha256" => $this->t("sha256 (recommended)"),
                "sha384" => $this->t("sha384"),
                "sha512" => $this->t("sha512"),
                "haval160,4" => $this->t("haval160,4"),
            ],
            "#default_value" => $config->get("algorithm"),
            "#required" => TRUE
        ];

        return parent::buildForm($form, $formState);
    }

    public function submitForm(array &$form, ?FormStateInterface $formState) {
        $values = $formState->getValues();

        $this->configFactory->getEditable("varnish_cache_purger.settings")
            ->set("salt", $values["salt"])
            ->set("token", $values["token"])
            ->set("ban_endpoint", $values["ban_endpoint"])
            ->set("algorithm", $values["algorithm"])
            ->save();

        parent::submitForm($form, $formState);
    }
}