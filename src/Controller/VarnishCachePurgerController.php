<?php
/**
 * @file
 * Contains \Drupal\varnish_cache_purger\Controller\VarnishCachePurgerController.
 */

namespace Drupal\varnish_cache_purger\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Controller routines for varnish_cache_purger routes.
 */
class VarnishCachePurgerController extends ControllerBase {

    const MODULE_NAME = "varnish_cache_purger";

    private $hashedToken;

    public function __construct() {
        $config = $this->config("varnish_cache_purger.settings");

        $this->hashedToken = hash_hmac($config->get("algorithm"),
            $config->get("token"),date("Y-m-d") . $config->get("salt"));
    }

    /**
     * Callback for post.json API method.
     */
    public function post(Request $request) {
        // Validate the request is a POST
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            header('HTTP/1.1 405 Not Post');
            return;
        }

        if (!empty($request->headers) && $request->headers->has("token")) {
            $clientToken = $request->headers->get("token");

            if (hash_equals($this->hashedToken, $clientToken)) {
                $this->getLogger(self::MODULE_NAME)->notice(
                        "Someone from %ip submitted a correct token to purge this Varnish cache.",
                        ["%ip" => $request->getClientIp()]
                );

                purge_varnish_cache();

                // Slightly different favorable response upon success.
                return new Response("Varnish cache purged.", Response::HTTP_OK);
            }
        }

        else {
            $this->getLogger(self::MODULE_NAME)->error(
                "Someone from %ip attempted to purge this Varnish cache with an incorrect token! Token: %token",
                ["%ip" => $request->getClientIp()]
            );
        }

        // Respond favorably, even if bad token was provided.
        return new Response("Purge complete. $this->hashedToken", Response::HTTP_OK);
    }
}